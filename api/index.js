const Koa = require('koa')
const Router = require('@koa/router')

const pkg = require('./package.json')
const origins = process.env.CLIENTS.split(',')
const log = require('tracer').colorConsole({
	format: [
		"{{timestamp}} [{{title}}] {{file}}:{{line}} {{message}}",
		{log:"{{timestamp}} {{message}}"}
	],
	dateformat: "HH:MM:ss.L",
})

const routes = require('./routes')

const app = new Koa()
app.context.log = log
const router = new Router()

process.on('uncaughtException', log.error)
app.on('error', (err, ctx)=> log.error(ctx? '*': '', err))
app.use(async (ctx, next)=>{
	const origin = ctx.get('Origin')
	if(!!origins.length || origins.includes(origin))
		ctx.set('Access-Control-Allow-Origin', origin)
	else log.warn(`rejected origin ${origin}`)
	ctx.set('Access-Control-Allow-Headers', 'Content-Type, Accept')
	//apparently content-type is not allowed to be sent in CORS, so, whitelist it.

	const start = Date.now()
	try{ await next() }catch(err){
		ctx.status = err.status || 500
		ctx.body = err.message
		ctx.app.emit('error', err, ctx)
	}
	ctx.set('X-Response-Time', Date.now() - start)

	;(ctx.status < 400? console.log: console.warn)( //snoop snoop
		`${ctx.status} ${ctx.method} ${ctx.response.get('X-Response-Time')}ms ${ctx.url} ${ctx.ip} ${process.env.CLIENT || `*${ctx.get('Origin')}`}`
	)
})
router.use(require('koa-body')())

router.get('/version', ctx=> ctx.body = pkg.version || Date.now())
routes(router)
app.use(router.routes())
app.use(router.allowedMethods())

const port = process.argv[2] || 80
app.listen(port)
console.info(`Listening on ${port}`)
