const yt = require('ytdl-core')
const ffmpeg = require('fluent-ffmpeg')
const moment = require('moment')

const getTags = ({name, author})=>{
	const [artist, title] = name.replace(/[\\/*?:"<>|]/g, '').split('-').map(s=> s.trim())
	const topic = author.match(/(.+) - Topic/i)
	return {
		title: title || artist,
		artist: topic? topic[1]: title? artist: ''
	}
}

module.exports = router=>{
	router.all(['/info', '/info/:id'], async ctx=>{
		if(ctx.method==='OPTIONS') return ctx.status = 200 //some CORS stuff idk
		const {search} = {...ctx.request.body, ...ctx.request.query}
		const id = ctx.params.id || yt.validateURL(search)? yt.getVideoID(search):
			yt.validateID(search)? search:
			false
		if(!id) return ctx.status = 404

		const info = await yt.getBasicInfo(id)
		const {title, artist} = getTags({
			name: info.videoDetails.title,
			author: info.videoDetails.author.name
		})
		ctx.params.title = title
		ctx.params.artist = artist
		ctx.body = {
			id,
			title, artist,
			embed: info.videoDetails.embed.iframeUrl,
			seconds: info.videoDetails.lengthSeconds
		}
	})
	router.all('/dl/:id', async ctx=>{
		const {title, artist, start, end} = {...ctx.request.body, ...ctx.request.query}
		const start2 = moment.duration(start || 0)
		const end2 = moment.duration(end || 0)
		const duration = moment.duration(end2).subtract(start)

		ctx.attachment(`${artist} - ${title}.mp3`)
		const video = yt(ctx.params.id, {filter: 'audio', quality: 'highest'})
		const converter = ffmpeg()
			.input(video).format('mp3')
			.seek(start2.asSeconds()).duration(duration.asSeconds())
			.outputOption('-metadata', `artist=${artist}`)
			.outputOption('-metadata', `title=${title}`)
			.outputOption('-metadata', `comment=${ctx.params.id}::${start2.asSeconds()}::${duration.asSeconds()}`)
			.on('error', err=>{throw err})
		ctx.body = converter.pipe()
	})
}
